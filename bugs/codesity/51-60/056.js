async function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

async function main() {
  const numbers = [1, 2, 3]
  const store = []

  const promises = numbers.map(async (num) => {
    await sleep(1000)
    store.push(num)
  })
  await Promise.all(promises)

  console.log(store)
}
main()

// https://codisity.pl/100-bugow-js/979ce6b4-71f8-4d13-80ea-8ab3a2b99a03
