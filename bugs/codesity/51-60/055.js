bar: {
  foo: {
    console.log("first")
    break foo
    console.log("second")
  }

  break bar

  console.log("third")
}

// labeling is used for break and continue statements

bar: {
  for (let i = 0; i < 5; i++) {
    console.log(i)
    if (i === 2) break bar;
  }
}

// https://codisity.pl/100-bugow-js/74f05a57-ccd6-44f8-a982-aacec55d6132