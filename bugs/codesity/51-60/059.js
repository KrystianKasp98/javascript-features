let foo = { val: 1 }
let bar = foo // bar = { val: 1 }
foo.prop = foo = { val: 2 } // foo is now reference of new object - { val: 2 }, but bar still points to old object - { val: 1, prop: { val: 2 } }

console.log(foo) // { val: 2 }
console.log(bar) // { val: 1, prop: { val: 2 } }
console.log(foo.prop) // undefined

// https://codisity.pl/100-bugow-js/b85b88a3-7e88-4091-a7f1-62ee08894050
