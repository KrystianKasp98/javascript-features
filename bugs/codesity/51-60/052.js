let a, b, c;
let x, y, z;

a = b = 1, c = 2;
x =(y = 1, z = 2, f = 3);

console.log({a});
console.log({b});
console.log({c});
console.log({x});
console.log({y});
console.log({z});

console.log("foo", "bar");
console.log(("foo", "bar"));

// it works like: const a = (first, second, third) => third;
// https://codisity.pl/100-bugow-js/e5651472-8df9-4bec-bea5-f9d5afb419bf