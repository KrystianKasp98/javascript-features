// returns spread arguments from function
const func1 = function (...arguments) {
    console.log(arguments);
}

// returns spread arguments from function
const func2 = (...arguments) => {
    console.log(arguments);
}

// returns arguments from function
const func3 = function (a, b) {
  console.log(arguments);
}

// loses reference to arguments from function, reference to nodejs globals arguments
const func4 = (a, b) => {
  console.log(arguments);
}

func1("foo", "bar");
func2("foo", "bar");
func3("foo", "bar");
func4("foo", "bar");

// https://codisity.pl/100-bugow-js/0e1bfcac-da2c-4569-9dde-eb6c281eef1a
