console.log(null || undefined)
console.log(undefined || null)
console.log(null ?? undefined)

console.log(null ?? "foo")
console.log(undefined ?? "foo")
console.log(0 ?? "foo")
console.log("" ?? "foo")
console.log(false ?? "foo")
console.log(NaN ?? "foo")
console.log(0 || "foo")
console.log("" || "foo")
console.log(false || "foo")
console.log(NaN || "foo")


console.log((null || undefined) ?? "foo")
// https://codisity.pl/100-bugow-js/bb05d968-36d2-4e0d-b17f-d60291b46bf3
