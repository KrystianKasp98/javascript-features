console.log(null == 0) // false
console.log(null > 0) // false
console.log(null >= 0) // true, operator >= converts null to 0
console.log(null == undefined) // true

// https://codisity.pl/100-bugow-js/df58179e-11c5-4987-9cf7-d513ef33ba6a