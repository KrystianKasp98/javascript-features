class Game {
  constructor() {
    this.wins = 0;
    this.losses = 0;
    this.total = 0;
    this.currentStreak = { win: 0, loss: 0 };
    this.maxStreak = { win: 0, loss: 0 };
  }

  #updateStreak(isWin) {
    const streakType = isWin ? 'win' : 'loss';
    const oppositeStreak = isWin ? 'loss' : 'win';

    this.currentStreak[streakType]++;
    this.currentStreak[oppositeStreak] = 0;
    this.maxStreak[streakType] = Math.max(this.maxStreak[streakType], this.currentStreak[streakType]);
  }

  #resolveTry(result) {
    this.total++;
    this.#updateStreak(result);
    result ? this.wins++ : this.losses++;
  }

  decideTries(count) {
    for (let i = 0; i < count; i++) {
      this.#resolveTry(Math.random() >= 0.5);
    }
  }

  getSummary() {
    return {
      wins: this.wins,
      losses: this.losses,
      total: this.total,
      maxWinStreak: this.maxStreak.win,
      maxLossStreak: this.maxStreak.loss,
      winRate: this.total ? `${(this.wins / this.total * 100).toFixed(2)}%` : "0%",
    };
  }
}

const game = new Game();
game.decideTries(10);
console.log(game.getSummary());
